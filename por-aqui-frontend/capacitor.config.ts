import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'pi.fatec.poraqui',
  appName: 'Por aqui Frontend',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
