import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  type;
  constructor(public auth: AuthenticationService) {}

  async ionViewWillEnter() {
    this.auth.storageGet('user').then(async i => {
      if (i) {
        this.type = i.type
      } else {
        this.auth.getUserByAuth(await this.auth.getAuthenticateState().then(e => e)).subscribe(e => {this.auth.storageSet('user', e); this.type = e.type})
      }
    });
  }

}
