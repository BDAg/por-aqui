import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  step = 0;
  selectOpen = false;
  profilePicture = null;
  user = {
    username: '',
    password: '',
    type: ''
  };

  constructor(public location: Location, public registerService: RegisterService) {
  }

  selectProfilePicture(event) {
    console.log(event.target.files)
    this.registerService.getUserByAuth(event.target.files[0]).subscribe(e => console.log(e));
  }

  goBack() {
    if (this.step === 0) {
      this.location.back();
    } else {
      this.step = this.step - 1;
    }
  }

  next() {
    if (this.step === document.querySelectorAll(".step").length - 1) {
      (this.user);
    } else {
      this.step = this.step + 1;
    }
  }

  triggerSelect() {
    this.selectOpen = !this.selectOpen;
    if (this.selectOpen) {
      document.getElementById('select').setAttribute("class", "select-inner open-select");
    } else {
      document.getElementById('select').setAttribute("class", "select-inner closed-select");
    }
  }

  translate(type) {
    return type === 'buyer' ? 'COMPRADOR' : type === 'merchant' ? 'COMERCIANTE' : null;
  }
  
  selectType() {  
    document.getElementById('select').focus();
  }

  pickType(event, type) {
    event.stopPropagation();
    document.getElementById('select').blur();
    this.user.type = type;
  }
}
