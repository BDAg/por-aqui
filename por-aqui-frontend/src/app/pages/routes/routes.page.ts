import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { SidemenuComponent } from '../../components/sidemenu/sidemenu.component';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-routes',
  templateUrl: 'routes.page.html',
  styleUrls: ['routes.page.scss']
})
export class RoutesPage {

  constructor(private modal: ModalController, private auth: AuthenticationService, private router: Router) {}

  logoff() {
    this.auth.logoff();
    this.router.navigate(['/'])
  }

  async openMenu() {
    (await this.modal.create({
      component: SidemenuComponent,
      swipeToClose: true,
      cssClass: 'menu',
      backdropDismiss: true
    })).present();
  }
}
