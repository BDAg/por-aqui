import { Component } from '@angular/core';
import * as Leaflet from 'leaflet';
import { antPath } from 'leaflet-ant-path';
import { RoutesService } from 'src/app/services/routes.service';
import { url } from '../../services/variables';

@Component({
  selector: 'app-map',
  templateUrl: 'map.page.html',
  styleUrls: ['map.page.scss']
})
export class MapPage {
  map: Leaflet.Map;

  constructor(public routesService: RoutesService) { }

  ngOnInit() { }
  ionViewDidEnter() { this.leafletMap(); }

  leafletMap() {
    this.map = Leaflet.map('mapId', null).setView([-23.55419267300477, -46.645725315718686], 5);
    this.map.zoomControl.remove();
    Leaflet.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', null).addTo(this.map);
    Leaflet.marker([-23.55419267300477, -46.645725315718686], {icon: Leaflet.icon({
      iconUrl: 'https://i.imgur.com/YepXB3o.png',
      shadowUrl: 'https://i.imgur.com/vwEg49E.png',
      iconSize: [41, 51],
      iconAnchor: [20, 51],
      popupAnchor: [0, -51]
    })}).addTo(this.map);
    Leaflet.marker([-23.56002223239146, -46.63523408270475], {icon: Leaflet.icon({
      iconUrl: 'https://i.imgur.com/YepXB3o.png',
      shadowUrl: 'https://i.imgur.com/vwEg49E.png',
      iconSize: [41, 51],
      iconAnchor: [20, 51],
      popupAnchor: [0, -51]
    })}).addTo(this.map);
    this.routesService.getDirections([-23.55419267300477, -46.645725315718686], [-23.56002223239146, -46.63523408270475])
      .subscribe(result => {
        console.log(result.features[0].geometry.coordinates)
        const coordinates = result.features[0].geometry.coordinates.map(e => e.reverse());
        console.log(coordinates)
        const path = antPath(coordinates, {
          "delay": 1,
          "dashArray": [
            1,
            1
          ],
          "weight": 3,
          "color": "#0000FF",
          "pulseColor": "#FFFFFF",
          "paused": false,
          "reverse": false,
          "hardwareAccelerated": true
        });
        this.map.addLayer(path);
        this.map.fitBounds(path.getBounds())
      });
  }

  /** Remove map when we have multiple map object */
  ngOnDestroy() {
    this.map.remove();
  }

}
