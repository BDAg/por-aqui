import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent {

  constructor(private auth: AuthenticationService, private modal: ModalController) { }

  confirm() {
    this.auth.setFirstTime();
    this.modal.dismiss();
  }

}
