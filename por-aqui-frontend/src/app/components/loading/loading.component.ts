import { Component, Input } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent {
  constructor(public loadingController: LoadingController) {}

  async dismiss(loading) {
    await loading.dismiss();
  }

  async presentLoading(options = null, loading) {
    loading = await this.loadingController.create(options || {
      spinner: 'circles'
    });
    return await loading.present();
  }
}
