import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { LandingPageComponent } from '../components/landing-page/landing-page.component';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  @ViewChild('user') user;
  @ViewChild('pass') pass;
  hidden = true;
  constructor(private auth: AuthenticationService, private router: Router, private modalController: ModalController) {
    this.auth.store.subscribe(async (val) => {
      console.log('store', val)
      if (val) {
        this.auth.getUserByAuth(await this.auth.getAuthenticateState().then(e => e)).subscribe(e => this.auth.storageSet('user', e));
        this.router.navigate(['/main/tabs/map']);
      }
    });
  }

  ionViewDidEnter() {
    this.auth.firstTime()
      .then(async firstTime => {
        if (!firstTime) {
          const modal = await this.modalController.create({
            component: LandingPageComponent
            });
          
            await modal.present();
        }
      })
  }

  register() {
    this.router.navigate(['/register']);
  }

  async authenticate() {
    await this.auth.authenticate({username: this.user.nativeElement.value, password: this.pass.nativeElement.value})
  }

}
