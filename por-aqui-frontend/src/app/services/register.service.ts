import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { url } from './variables';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(private http: HttpClient) { }

  getUserByAuth(file) {
    var formdata = new FormData();
    formdata.append("image", file);

    // fetch("https://api.imgur.com/3/upload", {
    //   method: 'POST',
    //   // headers: myHeaders,
    //   body: formdata,
    //   redirect: 'follow'
    // })
    //   .then(response => response.text())
    //   .then(result => console.log(result))
    //   .catch(error => console.log('error', error));
    return this.http.post<any>("https://api.imgur.com/3/upload", formdata, {headers: { 'Authorization': "Client-ID dab3f7ecf7793fb", "Content-Type": "application/form-data" }} );
  }
}
