import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { url } from './variables';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {
  constructor(private http: HttpClient) { }

  getDirections(start, end) {
    return this.http.get<any>(`https://api.openrouteservice.org/v2/directions/driving-car?api_key=${environment.OSM_KEY}&start=${start[1]},${start[0]}&end=${end[1]},${end[0]}`);
  }
}
