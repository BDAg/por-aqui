import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpClient } from '@angular/common/http';
import { LoadingController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { url } from './variables';
import { LoadingComponent } from '../components/loading/loading.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private storageObserver: any;
  public store: any;
  constructor(public loadingController: LoadingController, public loading: LoadingComponent, public toast: ToastController, private storageService: Storage, private http: HttpClient) {
    this.storageService.create();
    this.storageObserver= null;
    this.store= Observable.create(observer => {
        this.storageObserver= observer;
    });
  }

  getAuthByUser(user) {
    return this.http.post<any>(`${url}/user/auth`, user);
  }

  async authenticate(val) {
    const loading = await this.loadingController.create({
      spinner: 'circles'
    });
    loading.present();
    this.getAuthByUser(val)
    .subscribe(auth => {
        loading.dismiss();
        this.storageService.set('auth', auth)
        this.storageObserver.next(auth);
      },
      err => {
        loading.dismiss();
        this.toast.create({
          message: err.error.error,
          icon: 'warning-outline',
          color: 'warning',
          duration: 2000
        }).then(t =>  t.present());
      });
  }

  getUserByAuth(auth) {
    return this.http.get<any>(`${url}/user/validate/${auth}`);
  }

  async getAuthenticateState() {
    return await this.storageService.get('auth').then(auth => auth);
  }

  async logoff() {
    await this.storageService.remove('auth');
    await this.storageService.remove('user');
  }

  async firstTime() {
    return await this.storageService.get('first-time').then(e => e);
  }

  async setFirstTime() {
    await this.storageService.set('first-time', true);
  }

  async storageSet(e, val) {
    await this.storageService.set(e, val);
  };

  async storageGet(e) {
    return await this.storageService.get(e).then(e => e);
  };
  
  async storageRemove(e) {
    await this.storageService.remove(e);
  };
}
