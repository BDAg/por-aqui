# Por aqui

Aplicativo em Ionic Angular que une a praticidade de comprar localmente e a possibilidade de se organizar para manter uma trajeto eficiente, traz também a possibilidade de anunciantes mostrarem suas promoções locais para os moradores.

# Introdução

Devido a atual pandemia mundial, grande maioria das pessoas iniciaram e outras continuaram a fazer compras de modo on-line, através de diversos aplicativos e sites. Isso fez com que os comércios locais perdessem grande parte do fluxo.
Pensando nisso a iniciativa do aplicativo Por Aqui foi idealizada, basicamente se consiste em um app que busca lojas ao redor e apresenta seus respectivos itens a venda, através de cadastros de usuários de pessoa física ou pessoa jurídica.

# Especificações Técnicas

Nós faremos um aplicativo Web Progressivo, desenvolvido em Ionic com Angular para gerar interfaces rápidas, responsivas e reativas com componentes nativos de diversos celulares permitindo uma gama maior de aparelhos e consequentemente uma acessibilidade maior.
Para alimentar esta interface, utilizaremos uma linguagem de programação extremamente compatível que também é baseada em Javascript, a linguagem de sustentação conhecida como NodeJs, através dela faremos as requisições necessárias para criar as funcionalidades e permitir o uso dinâmico do aplicativo. Já para parte de armazenamento utilizaremos o banco de dados e gerenciador MSSQL/Management Studio.
Para o planejamento de Interface Visual e Mockup foi adotado o uso da Ferramenta Figma.
