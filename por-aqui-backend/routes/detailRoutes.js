const router = require('express').Router();
const crypto = require('crypto');
function getId(token) {    
    const decrypt = (hash) => {
        const decipher = crypto.createDecipheriv('aes-256-ctr', process.env.SECRET, Buffer.from(hash.iv, 'hex'));
        const decrypted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
        return decrypted.toString();
    };
    return JSON.parse(decrypt({
        iv: token.split('&')[0],
        content: token.split('&')[1],
    }))._id;
}

router.get('/validate/:token', (req, res) => {
    try {
        res.status(200).json(getId(req.params.token));
        // User.findOne({username}, (err, user) => {
        //     user.comparePassword(password, function(err, isMatch) {
        //         if (err) throw err;
        //         console.log(password, isMatch); // -&gt; Password123: true
        //         if (isMatch) {
        //             res.status(200).json({token: encrypt(JSON.stringify(user))});
        //         } else {
        //             res.status(422).json({error: "O usuário não foi encontado!"})
        //             return;
        //         }
        //     });
        // });
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});

module.exports = router;