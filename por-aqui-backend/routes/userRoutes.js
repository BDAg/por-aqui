const router = require('express').Router();
const User = require('../models/User');
const { DetailUser } = require('../models/Detail');
const crypto = require('crypto');
router.post('', async (req, res) => {
    const { username, password, type } = req.body;

    if (!username || !password || !type) {
        res.status(422).json({error: "Preencha todos os campos obrigatórios!"});
        return;
    }
    const user = { username, password, type };

    try {
        await User.create(user);
        // await DetailUser.create({profilePicture: ''});
        res.status(201).json({message: 'Usuário criado com sucesso.'});
    } catch (error) {
        res.status(500).json({error: error.message});
    }
});

router.get('', async (req, res) => {
    try {
        const users = await User.find();

        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({error: error.message});        
    }
});

router.get('/:id', async (req, res) => {
    try {
        const user = await User.findOne({_id: req.params.id});
        if (!user) {
            res.status(422).json({error: "O usuário não foi encontado!"})
        }

        res.status(200).json(user);
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});

router.patch('/:id', async (req, res) => {
    const user = req.body;
    try {
        const updatedUser = await User.updateOne({_id: req.params.id}, user);
        if (updatedUser.matchedCount === 0) { 
            res.status(422).json({error: "O usuário não foi encontado!"})
        }

        res.status(200).json(user);
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const deletedUser = await User.findByIdAndDelete({_id: req.params.id});
        console.log(deletedUser)
        if (!deletedUser) {
            res.status(422).json({error: "O usuário não foi encontado!"})
            return;
        }
        res.status(200).json({message: "Usuário excluído com sucesso!"});
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});
const algorithm = 'aes-256-ctr';
const iv = crypto.randomBytes(16);

function validate(token) {    
    const decrypt = (hash) => {
        const decipher = crypto.createDecipheriv('aes-256-ctr', process.env.SECRET, Buffer.from(hash.iv, 'hex'));
        const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
        return decrpyted.toString();
    };
    return JSON.parse(decrypt({
        iv: token.split('&')[0],
        content: token.split('&')[1],
    }))
}

const encrypt = (text) => {
    const cipher = crypto.createCipheriv('aes-256-ctr', process.env.SECRET, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return iv.toString('hex') + '&' + encrypted.toString('hex');
};

router.post('/auth', (req, res) => {
    const { username, password } = req.body;
    try {
        User.findOne({username}, (err, user) => {
            if (user) {
                user.comparePassword(password, function(err, isMatch) {
                    if (err) throw err;
                    if (isMatch) {
                        res.status(200).json(encrypt(JSON.stringify(user)));
                    } else {
                        res.status(422).json({error: "Erro na autenticação"})
                        return;
                    }
                });
            } else {
                res.status(422).json({error: "Erro na autenticação"})
            }
        });
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});

router.get('/validate/:token', (req, res) => {
    try {
        let result = validate(req.params.token);
        delete result.password;
        delete result.__v;
        res.status(200).json(result);
        // User.findOne({username}, (err, user) => {
        //     user.comparePassword(password, function(err, isMatch) {
        //         if (err) throw err;
        //         console.log(password, isMatch); // -&gt; Password123: true
        //         if (isMatch) {
        //             res.status(200).json({token: encrypt(JSON.stringify(user))});
        //         } else {
        //             res.status(422).json({error: "O usuário não foi encontado!"})
        //             return;
        //         }
        //     });
        // });
    } catch (error) {   
        res.status(500).json({error: error.message});        
    }
});

module.exports = router;