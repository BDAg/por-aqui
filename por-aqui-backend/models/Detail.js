var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
     
var DetailUserSchema = new Schema({
    profilePicture: { type: String },
    name: { type: String },
    doc: { type: String },
});

var DetailProductSchema = new Schema({
    image: { type: String },
    name: { type: String },
    price: { type: Number },
});
     
module.exports = [
    mongoose.model('DetailUser', DetailUserSchema),
    mongoose.model('DetailProduct', DetailProductSchema),
];